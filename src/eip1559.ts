import type { ProviderLike } from '@mycrypto/eth-scan';

import { getFeeHistory, getLatestBlock } from './provider';
import type { EstimationResult, FeeHistory, ExtendedError } from './types';
import { gwei, hexlify, max, roundToWholeGwei } from './utils';

const MAX_GAS_FAST = gwei(50000000n);

// How many blocks to consider for priority fee estimation
const FEE_HISTORY_BLOCKS = 10;
// Which percentile of effective priority fees to include
const FEE_HISTORY_PERCENTILE = 5;
// Which base fee to trigger priority fee estimation at
const PRIORITY_FEE_ESTIMATION_TRIGGER = gwei(100n);
// Returned if above trigger is not met
const DEFAULT_PRIORITY_FEE = gwei(3n);
// In case something goes wrong fall back to this estimate
export const FALLBACK_ESTIMATE = {
  maxFeePerGas: gwei(20n),
  maxPriorityFeePerGas: DEFAULT_PRIORITY_FEE,
  baseFee: undefined
};
const PRIORITY_FEE_INCREASE_BOUNDARY = 200; // %

// Cache for the last successful fee estimate
let lastSuccessfulEstimate: EstimationResult | null = null;

// Retry attempts
// This is set to 5 to give DNS 15 second ttl time to expire and receive
// a new IP address to test against. (Specific to rpc.pulsechain.com)
const RETRY_LIMIT = 5;
// Initial backoff duration in milliseconds
const INITIAL_BACKOFF_MS = 500;
// Multiplier for each retry
const BACKOFF_MULTIPLIER = 2;
// Maximum backoff duration in milliseconds
const MAX_BACKOFF_MS = 30000;

// Returns base fee multiplier percentage
const getBaseFeeMultiplier = (baseFee: bigint) => {
  if (baseFee <= gwei(40n)) {
    return 200n;
  } else if (baseFee <= gwei(100n)) {
    return 160n;
  } else if (baseFee <= gwei(200n)) {
    return 140n;
  } else {
    return 120n;
  }
};

const calculatePriorityFeeEstimate = (feeHistory?: FeeHistory) => {
  if (!feeHistory) {
    return null;
  }

  const rewards = feeHistory.reward
    ?.map((r) => BigInt(r[0]))
    .filter((r) => r > 0n)
    .sort();

  if (!rewards) {
    return null;
  }

  // Calculate percentage increases from between ordered list of fees
  const percentageIncreases = rewards.reduce<bigint[]>((acc, cur, i, arr) => {
    if (i === arr.length - 1) {
      return acc;
    }
    const next = arr[i + 1];
    const p = ((next - cur) / cur) * 100n;
    return [...acc, p];
  }, []);
  const highestIncrease = max(percentageIncreases);
  const highestIncreaseIndex = percentageIncreases.findIndex((p) => p === highestIncrease);

  // If we have big increase in value, we could be considering "outliers" in our estimate
  // Skip the low elements and take a new median
  const values =
    highestIncrease >= PRIORITY_FEE_INCREASE_BOUNDARY &&
    highestIncreaseIndex >= Math.floor(rewards.length / 2)
      ? rewards.slice(highestIncreaseIndex)
      : rewards;

  return values[Math.floor(values.length / 2)];
};

export const calculateFees = (baseFee: bigint, feeHistory?: FeeHistory): EstimationResult => {
  try {
    const estimatedPriorityFee = calculatePriorityFeeEstimate(feeHistory);

    const maxPriorityFeePerGas = max([estimatedPriorityFee ?? 0n, DEFAULT_PRIORITY_FEE]);

    const multiplier = getBaseFeeMultiplier(baseFee);

    const potentialMaxFee = (baseFee * multiplier) / 100n;
    const maxFeePerGas =
      maxPriorityFeePerGas > potentialMaxFee
        ? potentialMaxFee + maxPriorityFeePerGas
        : potentialMaxFee;

    if (maxFeePerGas >= MAX_GAS_FAST || maxPriorityFeePerGas >= MAX_GAS_FAST) {
      throw new Error('Estimated gas fee was much higher than expected, erroring');
    }

    return {
      maxFeePerGas: roundToWholeGwei(maxFeePerGas),
      maxPriorityFeePerGas: roundToWholeGwei(maxPriorityFeePerGas),
      baseFee
    };
  } catch (err) {
    const newError: ExtendedError = new Error(
      `calculateFees failed: ${(err as Error).message || 'unknown error'}`
    );
    // Attach the original error as a property to preserve detailed info or stack trace
    newError.originalError = err as Error;
    throw newError;
  }
};

export const estimateFees = async (provider: ProviderLike): Promise<EstimationResult> => {
  const startTime = Date.now();
  let lastError = null;
  let retryCount = 0;
  let backoffDuration = INITIAL_BACKOFF_MS;
  let baseFee, latestBlock, feeHistory;

  while (retryCount < RETRY_LIMIT) {
    try {
      latestBlock = await getLatestBlock(provider);

      if (!latestBlock.baseFeePerGas) {
        throw new Error('An error occurred while fetching current base fee, falling back');
      }

      baseFee = BigInt(latestBlock.baseFeePerGas);

      const blockNumber = BigInt(latestBlock.number);

      feeHistory =
        baseFee >= PRIORITY_FEE_ESTIMATION_TRIGGER
          ? await getFeeHistory(provider, hexlify(FEE_HISTORY_BLOCKS), hexlify(blockNumber), [
              FEE_HISTORY_PERCENTILE
            ])
          : undefined;

      // If we successfully get the fee history, we calculate the fees
      const fees = calculateFees(baseFee, feeHistory);
      // Update the cache with the latest successful estimate
      lastSuccessfulEstimate = fees;
      return fees; // Return the successfully calculated fees
    } catch (err) {
      retryCount += 1;

      // eslint-disable-next-line no-console
      console.warn(
        `estimateFees error: Beginning retry attempt ${retryCount} in ${backoffDuration} milliseconds. Error details:`,
        err
      );
      lastError = err;

      // Wait for backoff duration before retrying
      await new Promise((resolve) => setTimeout(resolve, backoffDuration));
      backoffDuration = Math.min(backoffDuration * BACKOFF_MULTIPLIER, MAX_BACKOFF_MS);
    }
  }
  // If all retries fail, log error and return the last successful estimate or the static fallback if none exist
  const totalTime = Date.now() - startTime;
  // Convert milliseconds to seconds and keep two decimal places
  const totalTimeInSeconds = (totalTime / 1000).toFixed(2);
  // eslint-disable-next-line no-console
  console.warn(
    `Fallback estimate used after ${retryCount} retries and a total time of ${totalTimeInSeconds} seconds due to an error in gas estimation:`,
    lastError
  );

  return lastSuccessfulEstimate || FALLBACK_ESTIMATE;
};

export interface ExtendedError extends Error {
  originalError?: Error;
}
